package com.tni.au.agus.enterprise.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BasicGrup implements Serializable{
	
	
	public static String PENGGUNA = "PENGGUNA";
	/**
	 * 
	 */
	private static final long serialVersionUID = 7698185522337232107L;
	@Column(name = "GROUP_NAME")
	private String groupName;
	@Column(name = "GROUP_CODE")
	private String groupCode;
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

}
