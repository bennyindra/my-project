package com.tni.au.agus.enterprise.dao;

import com.tni.au.agus.enterprise.entity.Pengguna;

public interface IPenggunaDAO {
	
	public Pengguna findForLogin(String username, String password);

}
