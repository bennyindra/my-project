package com.tni.au.agus.enterprise.service;

import java.util.List;

import com.tni.au.agus.enterprise.dao.BarangDAO;
import com.tni.au.agus.enterprise.entity.Barang;

public class BarangService {
	
	private static BarangDAO barangDAO;
	
	public  BarangService() {
		barangDAO = new BarangDAO();
	}
	
	public void update(Barang entity) {
		barangDAO.openCurrentSessionwithTransaction();
		barangDAO.update(entity);
		barangDAO.closeCurrentSessionwithTransaction();
	}

	
	public Barang findById(String id) {
		barangDAO.openCurrentSession();
		Barang barang = barangDAO.findById(id);
		barangDAO.closeCurrentSession();
		return barang;
	}

	
	public void delete(Barang entity) {
		barangDAO.openCurrentSessionwithTransaction();
		barangDAO.delete(entity);
		barangDAO.closeCurrentSessionwithTransaction();
	}

	
	public List<Barang> findAll() {
		barangDAO.openCurrentSession();
		List<Barang> barangs = barangDAO.findAll();
		barangDAO.closeCurrentSession();
		return barangs;
	}

	
	public void deleteAll() {
		barangDAO.openCurrentSessionwithTransaction();
		barangDAO.deleteAll();
		barangDAO.closeCurrentSessionwithTransaction();
	}
	
	public List<Barang> findAll(Barang barang) {
		barangDAO.openCurrentSession();
		List<Barang> barangs = barangDAO.findBarangWithAllCriteria(barang);
		barangDAO.closeCurrentSession();
		return barangs;
	}

	
}
