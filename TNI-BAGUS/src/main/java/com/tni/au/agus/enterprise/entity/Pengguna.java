package com.tni.au.agus.enterprise.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PENGGUNA")
public class Pengguna extends BasicEntity implements Serializable {

	/**
	 * for user login, author : PBIP
	 */
	private static final long serialVersionUID = 1935217123566189529L;
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";

	@Id
	@Column(name = "ID")
	private String id;
	@Column(name = "USERNAME")
	private String username;
	@Column(name = "FULL_NAME")
	private String fullName;
	@Column(name = "PASSWORD")
	private String password;
	@Column(name = "PHONE_NUMBER")
	private String phoneNumber;
	@ManyToOne
	@JoinColumn(name="FK_USER_GROUP" )
	private GrupPengguna userGroup;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public GrupPengguna getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(GrupPengguna userGroup) {
		this.userGroup = userGroup;
	}
	
	

}
