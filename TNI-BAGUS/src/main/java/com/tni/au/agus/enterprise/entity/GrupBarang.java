package com.tni.au.agus.enterprise.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GRUP_BARANG")
public class GrupBarang extends BasicGrup implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4967664430048298399L;
	@Id
	@Column(name="ID")
	private String ID;

}
