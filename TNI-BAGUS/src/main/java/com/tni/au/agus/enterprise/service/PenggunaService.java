package com.tni.au.agus.enterprise.service;

import java.util.List;

import com.tni.au.agus.enterprise.dao.PenggunaDAO;
import com.tni.au.agus.enterprise.entity.Pengguna;

public class PenggunaService {
	
	private static PenggunaDAO penggunaDAO;
	
	public PenggunaService() {
		penggunaDAO = new PenggunaDAO();
	}

	
	public void update(Pengguna entity) {
		penggunaDAO.openCurrentSessionwithTransaction();
		penggunaDAO.update(entity);
		penggunaDAO.closeCurrentSessionwithTransaction();
	}

	
	public Pengguna findById(String id) {
		penggunaDAO.openCurrentSession();
		Pengguna pengguna = penggunaDAO.findById(id);
		penggunaDAO.closeCurrentSession();
		return pengguna;
	}

	
	public void delete(Pengguna entity) {
		penggunaDAO.openCurrentSession();
		penggunaDAO.delete(entity);
		penggunaDAO.closeCurrentSessionwithTransaction();
	}

	
	public List<Pengguna> findAll() {
		penggunaDAO.openCurrentSession();
		List<Pengguna> penggunas = penggunaDAO.findAll();
		penggunaDAO.closeCurrentSession();
		return penggunas;
	}

	
	public void deleteAll() {
		penggunaDAO.openCurrentSessionwithTransaction();
		penggunaDAO.deleteAll();
		penggunaDAO.closeCurrentSessionwithTransaction();
	}
	
	public Pengguna findPenggunaForLogin(String username, String password){
		penggunaDAO.openCurrentSession();
		Pengguna pengguna = penggunaDAO.findForLogin(username, password);
		penggunaDAO.closeCurrentSession();
		return pengguna;
	}
	
	
}
