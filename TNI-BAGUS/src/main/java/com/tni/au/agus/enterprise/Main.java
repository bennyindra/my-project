package com.tni.au.agus.enterprise;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.tni.au.agus.enterprise.service.BarangService;
import com.tni.au.agus.enterprise.service.PenggunaService;
import com.tni.au.agus.enterprise.uidesign.OpeningDialog;

public class Main {
	
	private static SessionFactory sessionFactory;
	private static BarangService barangService;
	private static PenggunaService penggunaService;
	
	public static SessionFactory getsessionFactory(){
		return sessionFactory;
	}

	public static BarangService getBarangService(){
		return barangService;
	}
	
	public static PenggunaService getPenggunaService(){
		return penggunaService;
	}
	
	public static Boolean login = false;
	public static String grupPengguna = "";


	public static void main(String[] args) {
		sessionFactory = getSessionFactory();
		barangService = new BarangService();
		penggunaService = new PenggunaService();
		
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
        }
		JDialog loginDialog = new OpeningDialog(new JFrame(), true);
		loginDialog.setTitle("Opening Dialog");
		loginDialog.setVisible(true);
		
		System.exit(0);
	}
	
	private static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration
				.buildSessionFactory(builder.build());
		return sessionFactory;
	}

}
