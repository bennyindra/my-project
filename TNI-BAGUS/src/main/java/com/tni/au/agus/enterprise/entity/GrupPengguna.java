package com.tni.au.agus.enterprise.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GRUP_PENGGUNA")
public class GrupPengguna extends BasicGrup implements Serializable {
	
	public static String TARUNA = "TARUNA";
	public static String GENERAL = "GENERAL";
	public static String ADMIN = "ADMIN";

	/**
	 * 
	 */
	private static final long serialVersionUID = 3692722015763765074L;
	@Id
	@Column(name = "ID")
	private String id;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
