package com.tni.au.agus.enterprise.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BasicEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6161323191023706355L;

	public static String ID = "ID";

	@Column(name="CREATED_DATE")
	private Date createdDate;
	@Column(name="LAST_MODIFIED_DATE")
	private Date lastModifiedDate;
	@Column(name="CREATED_BY")
	private String createdBy;
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
	
}
