package com.tni.au.agus.enterprise.dao;

import java.util.List;

import org.hibernate.criterion.Restrictions;

import com.tni.au.agus.enterprise.entity.Pengguna;

public class PenggunaDAO extends BaseSessionFactory implements IBaseDAO<Pengguna, String>, IPenggunaDAO {

	public void update(Pengguna entity) {
		openCurrentSession().saveOrUpdate(entity);
	}

	public Pengguna findById(String id) {
		return (Pengguna) getCurrentSession().createCriteria(Pengguna.class).add(Restrictions.eq(Pengguna.ID, id))
				.uniqueResult();
	}

	public void delete(Pengguna entity) {
		getCurrentSession().delete(entity);
	}

	@SuppressWarnings("unchecked")
	public List<Pengguna> findAll() {
		return getCurrentSession().createCriteria(Pengguna.class).list();
	}

	public void deleteAll() {
		for (Pengguna pengguna : findAll()) {
			getCurrentSession().delete(pengguna);
		}

	}

	public Pengguna findForLogin(String username, String password) {
		return (Pengguna) getCurrentSession().createCriteria(Pengguna.class)
		.createAlias("userGroup", "grupPengguna")
		.add(Restrictions.eq(Pengguna.USERNAME, username))
		.add(Restrictions.eq("grupPengguna.groupName", password))//ini bukan password tapi grup pengguna
		.uniqueResult();
	}

}
