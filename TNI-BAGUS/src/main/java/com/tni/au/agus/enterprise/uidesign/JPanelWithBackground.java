/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tni.au.agus.enterprise.uidesign;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.imageio.ImageIO;

/**
 *
 * @author Renny
 */
public class JPanelWithBackground extends javax.swing.JPanel{
    /**
	 * 
	 */
	private static final long serialVersionUID = 868560974010837133L;
	private BufferedImage image;

    public JPanelWithBackground() {
        try {         
          InputStream url = JPanelWithBackground.class.getResourceAsStream("/image/opening_1.png");
          image = ImageIO.read(url);
       } catch (IOException ex) {
           System.err.printf(getToolTipText(), ex.getMessage());
       }

    }

    public JPanelWithBackground(BufferedImage image) {
        this.image = image;
    }
    
    

    @Override
    protected void paintComponent(Graphics grphcs) {
        super.paintComponent(grphcs); //To change body of generated methods, choose Tools | Templates.
        grphcs.drawImage(image, 0, 0, null); // see javadoc for more info on the parameters            
    
    }
    
    
    
    
    
    
}
