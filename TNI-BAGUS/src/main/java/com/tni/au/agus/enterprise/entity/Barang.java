package com.tni.au.agus.enterprise.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BARANG")
public class Barang extends BasicEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4234383147845880550L;
	@Id
	@Column(name="ID")
	private String id;
	@Column(name="NAMA_BARANG",nullable=false)
	private String namaBarang;
	@Column(name="KODE_BARANG")
	private String kodeBarang;
	@Column(name="JENIS_BARANG")
	private String jenisBarang;
	@Column(name="KONDISI")
	private String kondisi;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public String getJenisBarang() {
        return jenisBarang;
    }

    public void setJenisBarang(String jenisBarang) {
        this.jenisBarang = jenisBarang;
    }

    public String getKondisi() {
        return kondisi;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }

}
