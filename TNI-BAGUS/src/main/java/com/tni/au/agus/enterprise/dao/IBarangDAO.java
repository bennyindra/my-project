package com.tni.au.agus.enterprise.dao;

import java.util.List;

import com.tni.au.agus.enterprise.entity.Barang;

public interface IBarangDAO {

	List<Barang> findBarangWithAllCriteria(Barang barang);

}
