package com.tni.au.agus.enterprise.dao;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.tni.au.agus.enterprise.entity.Barang;

public class BarangDAO extends BaseSessionFactory implements IBaseDAO<Barang, String>, IBarangDAO{
	

	public void update(Barang entity) {
		if (entity.getId()==null||entity.getId().isEmpty()) {
			entity.setId(UUID.randomUUID().toString());
			getCurrentSession().save(entity);
		}else getCurrentSession().saveOrUpdate(entity);
		getCurrentSession().flush();
	}

	public Barang findById(String id) {
		return (Barang) getCurrentSession().get(Barang.class, id);
	}

	public void delete(Barang entity) {
		getCurrentSession().delete(entity);
	}

	@SuppressWarnings("unchecked")
	public List<Barang> findAll() {
		Criteria criteria = getCurrentSession().createCriteria(Barang.class);
		return criteria.list();
		
	}

	public void deleteAll() {
		for (Barang barang : findAll()) {
			getCurrentSession().delete(barang);
		}
	}
	
//	@Column(name="NAMA_BARANG",nullable=false)
//	private String namaBarang;
//	@Column(name="KODE_BARANG")
//	private String kodeBarang;
//	@Column(name="JENIS_BARANG")
//	private String jenisBarang;
//	@Column(name="KONDISI")
//	private String kondisi;
//	@ManyToOne
//	@JoinColumn(name="FK_GROUP_BARANG")
	
	@SuppressWarnings("unchecked")
	public List<Barang> findBarangWithAllCriteria(Barang barang) {
//		if (barang.getJenisBarang().equals("ALL")) {
			barang.setJenisBarang("%");
//		} 
		return getCurrentSession().createCriteria(Barang.class)
				.add(Restrictions.like("namaBarang", "%"+barang.getNamaBarang()+"%"))
				.add(Restrictions.like("kodeBarang", "%"+barang.getKodeBarang()+"%"))
				.add(Restrictions.like("jenisBarang", barang.getJenisBarang()))
				.add(Restrictions.like("kondisi", "%"+barang.getKondisi()+"%"))
				.list();
				
				
		
	}
	

}
